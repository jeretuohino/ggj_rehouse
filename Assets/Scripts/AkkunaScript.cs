﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AkkunaScript : MonoBehaviour
{
    /// <summary>
    /// akkuna (12) [1]
    /// eastern shape of "window"
    /// There was a small akkuna in the sauna.
    /// </summary>
    /// 

    public List<GameObject> plankGos;

    [SerializeField]
    GameObject plankGO;

    [SerializeField]
    GameEvent OnNailingIt;

    [SerializeField]
    GameScriptableObject gameMan;

    public int maxPlankAmount;
    public int planksInWindow;

    private void Awake()
    {
        gameMan.RegisterMe(this);
        foreach(GameObject plank in plankGos)
        {
            maxPlankAmount++;
            if (plank.activeInHierarchy)
            {
                planksInWindow++;
            }
        }
    }

    public bool IsDone()
    {
        if (planksInWindow >= maxPlankAmount)
        {
           // gameMan.UpdateProgress();
            return true;
        }
        else
        {
            return false;
        }
    }

    private void OnApplicationQuit()
    {
        gameMan.UnRegisterMe(this);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Delete))
        {
            DestroyWindow();
        }

        if(gameMan.ShouldIXplode())
        {
            DestroyWindow();
        }
    }

    bool EnableOne()
    {
        foreach(GameObject go in plankGos)
        {
            if(!go.activeInHierarchy)
            {
                go.SetActive(true);
                planksInWindow++;
                return true;
            }
        }
        return false;
    }

    public void DestroyWindow()
    {
        int amountRemoved = 0;
        foreach(GameObject go in plankGos)
        {
            if (go.activeInHierarchy == true)
            {
                planksInWindow--;
                amountRemoved++;
                go.SetActive(false);
                GameObject newGo = Instantiate(plankGO, transform.position, Quaternion.identity);
                newGo.tag = "Untagged";
                newGo.transform.rotation = transform.parent.rotation;
                newGo.transform.Rotate(Vector3.up, 90f);
                newGo.GetComponent<Rigidbody>().velocity = -transform.forward * 20f;
                newGo.GetComponent<Rigidbody>().AddTorque(Random.insideUnitCircle.normalized * 120f);

                newGo.AddComponent<DestroyScript>();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Plank")
        {
            if(EnableOne())
            {
                Destroy(other.gameObject);
                OnNailingIt.Raise();
            }
        }
    }

}
