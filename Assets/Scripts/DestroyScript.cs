﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyScript : MonoBehaviour
{
    public float timeTillDeath = 20f;
    private float lifetime;
    // Update is called once per frame
    void Update()
    {
        lifetime += Time.deltaTime;
        if(lifetime > timeTillDeath)
        {
            Destroy(gameObject);
        }
    }
}
