﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerInteractScript : MonoBehaviour
{
    [SerializeField] GameObject plankGO;
    [SerializeField] float yeetForce;
    [SerializeField] float rotateDuringYeetingForce;
    [SerializeField] Transform plankSpot;

    List<GameObject> carryPlanks = new List<GameObject>();
    int amountOfPlanks;

    AudioManager audioMan;

    [SerializeField]
    GameObject kurkkuGO;
    [SerializeField] GameObject kurkkuSpot;
    bool KurkkuMopo = false;

    [SerializeField] TextMeshProUGUI textUI;

    [SerializeField] GameEvent OnPickup;
    [SerializeField] GameEvent OnPickupKurkku;
    [SerializeField] GameEvent OnThrow;
    [SerializeField] GameEvent OnThrowKurkku;

    int tutorialStep = 0;

    private void FixedUpdate()
    {
        if(carryPlanks.Count < amountOfPlanks)
        {
            GameObject go =Instantiate(plankGO, plankSpot.position + transform.up * 0.14f * carryPlanks.Count, plankSpot.rotation);
            go.transform.parent = transform;
            Destroy(go.GetComponent<BoxCollider>());
            Destroy(go.GetComponent<Rigidbody>());
            carryPlanks.Add(go);
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (tutorialStep == 1)
            {
                tutorialStep++;
                UpdateTutoriaL();
            }

            if (KurkkuMopo)
            {
                kurkkuSpot.SetActive(false);
                KurkkuMopo = false;

                GameObject go = Instantiate(kurkkuGO, transform.position, Quaternion.identity);
                go.transform.rotation = transform.parent.rotation;
                go.transform.Rotate(Vector3.up, 90f);
                go.GetComponent<Rigidbody>().velocity = transform.forward * yeetForce;
                go.GetComponent<Rigidbody>().AddTorque(Random.insideUnitCircle.normalized * rotateDuringYeetingForce);
                OnThrowKurkku.Raise();
                return;
            }

            if (amountOfPlanks > 0)
            {
                Destroy(carryPlanks[carryPlanks.Count - 1]);
                carryPlanks.RemoveAt(carryPlanks.Count - 1);
                amountOfPlanks--;

                GameObject go = Instantiate(plankGO, transform.position, Quaternion.identity);
                go.transform.rotation = transform.parent.rotation;
                go.transform.Rotate(Vector3.up, 90f);
                go.GetComponent<Rigidbody>().velocity = transform.forward * yeetForce;
                go.GetComponent<Rigidbody>().AddTorque(Random.insideUnitCircle.normalized * rotateDuringYeetingForce);
                OnThrow.Raise();
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position, transform.forward, out hit, 2f))
            {
                if (hit.transform.tag == "PlankPile")
                {
                    PickUpPlank(hit);
                }
                if (hit.transform.tag == "KurkkuVitunMopo")
                {
                    PickUpKurkkumopo(hit);
                }
            }
        }
    }

    public void PickUpPlank(RaycastHit hit)
    {
        amountOfPlanks++;
        if (tutorialStep == 0)
        {
            tutorialStep++;
            UpdateTutoriaL();
        }

        OnPickup.Raise();
    }

    public void PickUpKurkkumopo(RaycastHit hit)
    {
        kurkkuSpot.SetActive(true);
        KurkkuMopo = true;
        Destroy(hit.transform.gameObject);
        if (tutorialStep == 0)
        {
            tutorialStep++;
            UpdateTutoriaL();
        }

        OnPickupKurkku.Raise();
    }

    void UpdateTutoriaL()
    {
        switch (tutorialStep)
        {
            case 0:
                textUI.text = "Press E to pick up a plank from the pile";
                break;
            case 1:
                textUI.text = "Press Left Mouse Button to throw the plank away";
                break;
            default:
                textUI.enabled = false;
                break;
        }
    }
}
