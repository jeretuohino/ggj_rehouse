﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountDownTimer : MonoBehaviour
{
    [SerializeField] float timer = 10;
    float timeReset;


    [SerializeField]
    GameScriptableObject gameMan;


    // Start is called before the first frame update
    void Start()
    {

        timeReset = timer;
        timer = 4f;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        if (timer < 0)
        {
            gameMan.DestroyARandomWindow();
            timer = timeReset;
        }
    }
}
