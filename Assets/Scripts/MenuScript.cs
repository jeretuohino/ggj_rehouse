﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    //Moi Joni :)
    //Moi Matti :)
    //Moi Juhani :)
    //Moi Mitri :)
    //Moi Veeti :)
    //Moi Saara :)
    //Moi Iina :)
    //Moi Aada :)

    public Button creditsButton, startGameButton, exitGameButton;
    public GameObject mainMenuGO, creditsGO;

    // Start is called before the first frame update
    void Start()
    {
        startGameButton.onClick.AddListener(StartGame);
        creditsButton.onClick.AddListener(Credits);
        exitGameButton.onClick.AddListener(QuitGame);
    }

    public void StartGame()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Credits()
    {
        mainMenuGO.SetActive(false);
        creditsGO.SetActive(true);
    }

    public void Menu()
    {
        mainMenuGO.SetActive(true);
        creditsGO.SetActive(false);
    }
    
}
