﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BGMusicScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void OnLevelWasLoaded(int level)
    {
        if (SceneManager.GetSceneByBuildIndex(level).name == "SampleScene")
        {
            Debug.Log("We in Real Scene Boys!");
            GetComponent<AudioSource>().volume = GetComponent<AudioSource>().volume / 3;
        }
    }
}
