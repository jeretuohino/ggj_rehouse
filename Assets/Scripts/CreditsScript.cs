﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CreditsScript : MonoBehaviour
{
    //Moi Joni :)
    //Moi Matti :)
    //Moi Juhani :)
    //Moi Mitri :)
    //Moi Veeti :)
    //Moi Saara :)
    //Moi Iina :)
    //Moi Aada :)

    public Button menuButton;

    // Start is called before the first frame update
    void Start()
    {
        menuButton.onClick.AddListener(OpenMenu);
    }

    void OpenMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
