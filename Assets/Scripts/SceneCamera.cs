﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneCamera : MonoBehaviour
{
    [SerializeField] PlayerMovement player;

    public void EnablePlayer()
    {
        gameObject.SetActive(false);
        player.enabled = true;
        player.GetComponentInChildren<FirstPersonCamera>().enabled = true;
    }

    public void DisablePlayer()
    {
        gameObject.SetActive(true);
        player.enabled = false;
        player.GetComponentInChildren<FirstPersonCamera>().enabled = false;
    }

}
