﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PleaseReturnMrKurkkumopo : MonoBehaviour
{
    // Update is called once per frame
    void FixedUpdate()
    {
        if(transform.position.y < -100f)
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            transform.position = new Vector3(-1.87f, 20f, 4.43f);
            Instantiate(gameObject, transform.position + Vector3.left, Quaternion.identity);
        }
    }
}
