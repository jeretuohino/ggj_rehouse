﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    CharacterController cc;
    Rigidbody rb;

    bool onLadder = false;

    public float moveSpeed = 5f;
    private float translation;
    private float strafe;

    private bool cursorVisible = true;

    // Use this for initialization
    void Start()
    {
        // turn off the cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Input.GetAxis() is used to get the user's input
        // You can furthor set it on Unity. (Edit, Project Settings, Input)
        translation = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;
        strafe = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;

        if (!onLadder)transform.Translate(strafe, 0, translation);
        if (onLadder) transform.Translate(strafe, translation, 0);

        if (Input.GetKeyDown("escape"))
        {
            if (cursorVisible)
            {
                cursorVisible = false;
                // turn on the cursor
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                cursorVisible = true;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }

        if (transform.position.y < -200f)
        {
            transform.position = new Vector3(0, 40, 0);
            rb.velocity = Vector3.zero;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.transform.tag == "Ladder")
        {
            //Debug.Log("LADDDEEER");
            rb.useGravity = false;
            onLadder = true;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if(other.transform.tag == "Ladder")
        {
            //Debug.Log("ladddeer....");
            rb.useGravity = true;
            onLadder = false;
        }
    }
}
