﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpawnManagerScriptableObject", order = 1)]
public class GameScriptableObject : ScriptableObject
{
    public int maxAmountOfPlanks = 0;
    public int amountOfPlanks = 0;

    public List<AkkunaScript> Akkunat = new List<AkkunaScript>();

    [SerializeField] GameEvent OnWindowExplode;
    [SerializeField] GameEvent OnGameWin;
    [SerializeField] GameEvent OnGameLose;

    public void DestroyARandomWindow()
    {
        int rnumber = Random.Range(0, Akkunat.Count);
        Akkunat[rnumber].GetComponent<AkkunaScript>().DestroyWindow();
        OnWindowExplode.Raise();
        OnGameLose.Raise();
    }

    public bool ShouldIXplode()
    {
        if (maxAmountOfPlanks > 0)
        {
            if ((float)(amountOfPlanks / maxAmountOfPlanks) > 0.8f)
            {
                return true;
            }
        }
        return false;
    }

    public void RegisterMe(AkkunaScript akkuna)
    {
        Akkunat.Add(akkuna);
    }

    public void UnRegisterMe(AkkunaScript akkuna)
    {
        Akkunat.Remove(akkuna);
    }

}
